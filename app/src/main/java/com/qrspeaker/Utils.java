package com.qrspeaker;

import android.content.Context;

import com.qrspeaker.database.Message;

import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static java.lang.Math.abs;

public class Utils {

    private static final int PLAIN = 0;
    private static final int TYPICAL = 1;
    private static final int NOT_TYPICAL = 2;
    private static final int LANG = 3;
    public static Context context = null;
    static Locale probableLocale = null;
    private static String[] en = {"or", "his", "not", "but", "let", "the", "is", "was", "to", "under", "if", "above", "what", "who", "why", "do", "been", "has", "have", "which", "were", "did", "does", "a", "able", "about", "across", "add", "after", "afternoon", "again", "against", "age", "ago", "agree", "air", "all", "allow", "almost", "already", "also", "although", "always", "among", "and", "animal", "answer", "any", "anybody", "anything", "apple", "April", "arm", "around", "art", "as", "ask", "at", "August", "aunt", "away", "back", "bad", "bag", "ball", "bank", "be", "beautiful", "because", "become", "bed", "before", "begin", "behind", "believe", "best", "better", "between", "big", "bird", "black", "blue", "body", "book", "boy", "breakfast", "bridge", "bring", "brother", "build", "building", "bus", "business", "busy", "but", "buy", "by", "bye", "call", "can", "capital", "car", "cat", "chair", "change", "chicken", "child", "choose", "city", "clean", "clear", "clever", "close", "clothes", "coffee", "cold", "college", "color", "come", "company", "continue", "control", "cost", "country", "cry", "cup", "cut", "dance", "daughter", "day", "death", "December", "decide", "decision", "die", "different", "difficult", "do", "doctor", "dog", "door", "down", "draw", "dream", "dress", "drink", "drive", "dry", "during", "each", "east", "easy", "eat", "eight", "else", "end", "English", "enough", "even", "evening", "ever", "every", "everything", "explain", "eye", "face", "fall", "family", "far", "fast", "father", "favorite", "February", "feel", "few", "find", "fire", "fish", "five", "floor", "flower", "fly", "follow", "food", "foot", "for", "force", "forest", "former", "four", "free", "Friday", "friend", "from", "game", "garden", "get", "girl", "give", "go", "good", "goodbye", "government", "great", "green", "group", "grow", "hair", "hand", "happen", "happy", "hard", "hate", "have", "he", "head", "health", "hear", "heart", "heavy", "hello", "help", "here", "hi", "high", "hold", "home", "hope", "hospital", "hot", "hotel", "hour", "house", "how", "however", "human", "husband", "I", "idea", "if", "important", "in", "information", "interest", "into", "island", "it", "January", "job", "July", "June", "just", "keep", "key", "kid", "kill", "kind", "know", "large", "last", "law", "learn", "leave", "left", "leg", "less", "let", "letter", "library", "lie", "life", "light", "like", "listen", "little", "live", "long", "look", "lose", "lot", "love", "low", "main", "make", "man", "many", "March", "may", "May", "maybe", "mean", "meet", "minute", "moment", "Monday", "money", "month", "more", "morning", "most", "mother", "move", "much", "music", "must", "name", "near", "need", "never", "new", "news", "next", "night", "nine", "no", "nobody", "north", "nothing", "November", "now", "number", "October", "of", "off", "offer", "often", "OK", "old", "on", "one", "only", "open", "or", "other", "out", "over", "own", "paper", "parent", "park", "part", "pay", "pen", "people", "perfect", "person", "phone", "picture", "place", "play", "please", "police", "poor", "possible", "power", "price", "problem", "put", "question", "quick", "rain", "read", "real", "really", "red", "remember", "return", "right", "river", "road", "room", "run", "safe", "same", "Saturday", "say", "school", "sea", "second", "see", "seem", "sell", "send", "September", "serve", "service", "set", "seven", "several", "she", "ship", "shop", "short", "should", "show", "since", "sing", "sister", "sit", "six", "sky", "sleep", "slow", "small", "smile", "so", "some", "somebody", "something", "sometimes", "son", "song", "sorry", "south", "speak", "sport", "spring", "stand", "star", "start", "stay", "still", "stop", "story", "street", "strong", "student", "study", "such", "summer", "sun", "Sunday", "sure", "table", "take", "talk", "tall", "tea", "teach", "teacher", "tell", "ten", "test", "than", "thank", "that", "the", "then", "there", "they", "thing", "think", "this", "though", "three", "through", "throw", "Thursday", "time", "to", "today", "together", "tomorrow", "too", "touch", "town", "travel", "tree", "try", "Tuesday", "turn", "two", "under", "understand", "until", "up", "use", "very", "voice", "wait", "walk", "wall", "want", "war", "watch", "water", "way", "we", "wear", "Wednesday", "week", "welcome", "well", "west", "what", "when", "where", "which", "while", "white", "who", "why", "wife", "will", "win", "wind", "window", "winter", "wish", "with", "without", "woman", "word", "work", "world", "worry", "write", "year", "yes", "yesterday", "yet", "you", "young"};
    private static String[] fr = {"le", "de", "un", "être", "et", "à", "il", "avoir", "ne", "je", "son", "que", "se", "qui", "ce", "dans", "en", "du", "elle", "au", "de", "ce", "le", "pour", "pas", "que", "vous", "par", "sur", "faire", "plus", "dire", "me", "on", "mon", "lui", "nous", "comme", "mais", "pouvoir", "avec", "tout", "y", "aller", "voir", "en", "bien", "où", "sans", "tu", "ou", "leur", "homme", "si", "deux", "mari", "moi", "vouloir", "te", "femme", "venir", "quand", "grand", "celui", "si", "notre", "devoir", "là", "jour", "prendre", "même", "votre", "tout", "rien", "petit", "encore", "aussi", "quelque", "dont", "tout", "mer", "trouver", "donner", "temps", "ça", "peu", "même", "falloir", "sous", "parler", "alors", "main", "chose", "ton", "mettre", "vie", "savoir", "yeux", "passer", "autre", "la", "les", "Adjectifs", "bleu", "super", "autre", "bizarre", "difficile", "drôle", "étrange", "facile", "grave", "impossible", "jeune", "juste", "libre", "malade", "même", "pauvre", "possible", "propre", "rouge", "sale", "simple", "tranquille", "triste", "vide", "bonne", "toute", "doux", "faux", "français", "gros", "heureux", "mauvais", "sérieux", "vieux", "vrai", "ancien", "beau", "blanc", "certain", "chaud", "cher", "clair", "content", "dernier", "désolé", "différent", "droit", "entier", "fort", "froid", "gentil", "grand", "haut", "humain", "important", "joli", "léger", "long", "meilleur", "mort", "noir", "nouveau", "pareil", "petit", "plein", "premier", "prêt", "prochain", "quoi", "seul", "tout", "vert", "vivant", "Noms", "aide", "chef", "enfant", "garde", "gauche", "geste", "gosse", "livre", "merci", "mort", "ombre", "part", "poche", "professeur", "tour", "fois", "madame", "paix", "voix", "affaire", "année", "arme", "armée", "attention", "balle", "boîte", "bouche", "carte", "cause", "chambre", "chance", "chose", "classe", "confiance", "couleur", "cour", "cuisine", "dame", "dent", "droite", "école", "église", "envie", "épaule", "époque", "équipe", "erreur", "espèce", "face", "façon", "faim", "famille", "faute", "femme", "fenêtre", "fête", "fille", "fleur", "force", "forme", "guerre", "gueule", "habitude", "heure", "histoire", "idée", "image", "impression", "jambe", "joie", "journée", "langue", "lettre", "lèvre", "ligne", "lumière", "main", "maison", "maman", "manière", "marche", "merde", "mère", "minute", "musique", "nuit", "odeur", "oreille", "parole", "partie", "peau", "peine", "pensée", "personne", "peur", "photo", "pièce", "pierre", "place", "police", "porte", "présence", "prison", "putain", "question", "raison", "réponse", "robe", "route", "salle", "scène", "seconde", "sécurité", "semaine", "situation", "soeur", "soirée", "sorte", "suite", "table", "terre", "tête", "vérité", "ville", "voiture", "avis", "bois", "bras", "choix", "corps", "cours", "gars", "mois", "pays", "prix", "propos", "sens", "temps", "travers", "vieux", "accord", "agent", "amour", "appel", "arbre", "argent", "avenir", "avion", "bateau", "bébé", "besoin", "bonheur", "bonjour", "bord", "boulot", "bout", "bruit", "bureau", "café", "camp", "capitaine", "chat", "chemin", "chéri", "cheval", "cheveu", "chien", "ciel", "client", "cœur", "coin", "colonel", "compte", "copain", "côté", "coup", "courant", "début", "départ", "dieu", "docteur", "doigt", "dollar", "doute", "droit", "effet", "endroit", "ennemi", "escalier", "esprit", "état", "être", "exemple", "fait", "film", "flic", "fond", "français", "frère", "front", "garçon", "général", "genre", "goût", "gouvernement", "grand", "groupe", "haut", "homme", "honneur", "hôtel", "instant", "intérêt", "intérieur", "jardin", "jour", "journal", "lieu", "long", "maître", "mari", "mariage", "matin", "médecin", "mètre", "milieu", "million", "moment", "monde", "monsieur", "mouvement", "moyen", "noir", "nouveau", "numéro", "oeil", "oiseau", "oncle", "ordre", "papa", "papier", "parent", "passage", "passé", "patron", "père", "petit", "peuple", "pied", "plaisir", "plan", "point", "pouvoir", "premier", "présent", "président", "prince", "problème", "quartier", "rapport", "regard", "reste", "retard", "retour", "rêve", "revoir", "salut", "sang", "secret", "seigneur", "sentiment", "service", "seul", "siècle", "signe", "silence", "soir", "soldat", "soleil", "sourire", "souvenir", "sujet", "téléphone", "tout", "train", "travail", "trou", "truc", "type", "vent", "ventre", "verre", "village", "visage", "voyage", "fils", "gens", "Verbes", "abandonner", "accepter", "accompagner", "acheter", "adorer", "agir", "aider", "aimer", "ajouter", "aller", "amener", "amuser", "annoncer", "apercevoir", "apparaître", "appeler", "apporter", "apprendre", "approcher", "arranger", "arrêter", "arriver", "asseoir", "assurer", "attaquer", "atteindre", "attendre", "avancer", "avoir", "baisser", "battre", "boire", "bouger", "brûler", "cacher", "calmer", "casser", "cesser", "changer", "chanter", "charger", "chercher", "choisir", "commencer", "comprendre", "compter", "conduire", "connaître", "continuer", "coucher", "couper", "courir", "couvrir", "craindre", "crier", "croire", "danser", "décider", "découvrir", "dégager", "demander", "descendre", "désoler", "détester", "détruire", "devenir", "deviner", "devoir", "dire", "disparaître", "donner", "dormir", "échapper", "écouter", "écrire", "éloigner", "embrasser", "emmener", "empêcher", "emporter", "enlever", "entendre", "entrer", "envoyer", "espérer", "essayer", "être", "éviter", "excuser", "exister", "expliquer", "faire", "falloir", "fermer", "filer", "finir", "foutre", "frapper", "gagner", "garder", "glisser", "habiter", "ignorer", "imaginer", "importer", "inquiéter", "installer", "intéresser", "inviter", "jeter", "jouer", "jurer", "lâcher", "laisser", "lancer", "lever", "lire", "maintenir", "manger", "manquer", "marcher", "marier", "mener", "mentir", "mettre", "monter", "montrer", "mourir", "naître", "obliger", "occuper", "offrir", "oser", "oublier", "ouvrir", "paraître", "parler", "partir", "passer", "payer", "penser", "perdre", "permettre", "plaire", "pleurer", "porter", "poser", "pousser", "pouvoir", "préférer", "prendre", "préparer", "présenter", "prévenir", "prier", "promettre", "proposer", "protéger", "quitter", "raconter", "ramener", "rappeler", "recevoir", "reconnaître", "réfléchir", "refuser", "regarder", "rejoindre", "remarquer", "remettre", "remonter", "rencontrer", "rendre", "rentrer", "répéter", "répondre", "reposer", "reprendre", "ressembler", "rester", "retenir", "retirer", "retourner", "retrouver", "réussir", "réveiller", "revenir", "rêver", "revoir", "rire", "risquer", "rouler", "sauter", "sauver", "savoir", "sembler", "sentir", "séparer", "serrer", "servir", "sortir", "souffrir", "sourire", "souvenir", "suffire", "suivre", "taire", "tendre", "tenir", "tenter", "terminer", "tirer", "tomber", "toucher", "tourner", "traîner", "traiter", "travailler", "traverser", "tromper", "trouver", "tuer", "utiliser", "valoir", "vendre", "venir", "vivre", "voir", "voler", "vouloir"};
    private static String[] it = {"o", "gente", "persona", "uomo", "donna", "bambino", "ragazzo", "ragazza", "amico", "ospite", "famiglia", "genitori", "padre", "madre", "marito", "moglie", "figlio", "figlia", "nonno", "nonna", "suocero", "suocera", "zio", "zia", "fratello", "sorella", "cugino", "cugina", "lavoro", "insegnante", "autista", "operaio", "ingegnere", "medico", "infermiera", "commessa", "contabile", "pittore", "studente", "paese", "Russia", "Italia", "animale", "gatto", "cane", "uccello", "città", "scuola", "teatro", "strada", "piazza", "luogo", "casa", "hiesa", "fiume", "caffè", "albergo", "giardino", "parco", "banca", "fermata", "cinema", "ponte", "intersezione", "foresta", "ospedale", "mercato", "polizia", "posta", "stazione", "centro", "negozio", "montagna", "appartamento", "cucina", "balcone", "bagno", "doccia", "toilette", "pavimento", "piano", "corridoio", "cameradaletto", "salotto", "porta", "finestra", "chiave", "letto", "soffitto", "coperta", "cuscino", "tavola", "sedia", "poltrona", "frigo", "divano", "specchio", "cibo", "pane", "burro", "formaggio", "salsiccia", "olio", "pepe", "sale", "bacca", "miele", "marmellata", "fungo", "cipolla", "banana", "carota", "pera", "barbabietola", "frutto", "melone", "anguria", "torta", "cioccolato", "carne", "patata", "insalata", "pomodoro", "cetriolo", "cavolo", "cascia", "zuppa", "panino", "soda", "acqua", "caffè", "tè", "latte", "succo", "mela", "uva", "arancione", "ananas", "albicocca", "zucchero", "riso", "tagliatelle", "manzo", "maiale", "pollo", "cotoletta", "limone", "pisello", "ciambella", "pesce", "caramella", "gelato", "noce", "uovo", "pesco", "tazza", "bicchiere", "piatto", "cucchiaio", "forcetta", "coltello", "piattino", "bottiglia", "tovagliolo", "colazione", "pranzo", "cena", "aereo", "auto", "tram", "autobus", "treno", "bicicletta", "tempo", "anno", "settimana", "ora", "minuto", "ieri", "oggi", "domani", "festa", "volta", "pioggia", "vento", "neve", "cielo", "giorno", "mattina", "pomeriggio", "sera", "notte", "lunedi", "martedì", "mercoledì", "giovedi", "venerdì", "sabato", "domenica", "mese", "gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre", "primavera", "estate", "autunno", "inverno", "nome", "cognome", "indirizzo", "numero", "compleanno", "sposato", "cosa", "penna", "libro", "scacchi", "telefono", "orologio", "pettine", "televisore", "ferrodastiro", "sapone", "radio", "borsa", "cartolina", "mappa", "valigia", "regalo", "macchinafotografica", "videocamera", "lettore", "film", "fiore", "vaso", "quadro", "fazzoletto", "palla", "palloncino", "giocattolo", "conto", "busta", "carta", "giornale", "lettera", "biglietto", "scarpe", "cappotto", "vestito", "camicia", "gonna", "guanto", "cappello", "berretto", "giacca", "sciarpa", "calzino", "maglione", "maglietta", "cravatta", "pantaloni", "canzone", "musica", "taglia", "soldi", "felicità", "fortuna", "scherzo", "sorpresa", "problema", "checosa", "che", "chi", "dove", "didove", "come", "perché", "quando", "io", "tu", "lui", "lei", "Lei", "noi", "voi", "loro", "mio", "tuo", "suo", "nostro", "vostro", "da", "di.", "a", "in", "su", "sotto", "dietro", "con", "senza", "prima", "dopo", "tra", "vicinoa", "per", "uno", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove", "dieci", "undici", "dodici", "tredici", "quattordici", "quindici", "sedici", "diciassette", "diciotto", "diciannove", "venti", "trenta", "quaranta", "cinquanta", "sessanta", "settanta", "ottanta", "novanta", "cento", "mille", "vecchio", "giovane", "nuovo", "grande", "piccolo", "fame", "sazio", "buono", "bene", "cattivo", "male", "prestoo", "tardi", "ultimo", "prossimo", "libero", "gratis", "caldo", "freddo", "alto", "basso", "lungo", "facile", "difficile", "scuro", "chiaro", "caro", "economico", "asinistra", "adestra", "giusto", "rapido", "lento", "morbido", "duro", "bello", "attento", "triste", "felice", "felice", "pronto", "arrabbiato", "principale", "nero", "blu", "azzurro", "marrone", "verde", "grigio", "rosso", "bianco", "giallo", "sì", "no", "non", "questo", "quello", "qui", "lì", "adesso", "già", "ancora", "molto", "tanto", "poco", "ogni", "tutti", "tutto", "e", "o", "perché", "essere", "stare", "avere", "potere", "dovere", "volere", "vivere", "abitare", "venire", "andare", "guidare", "vedere", "sedere", "dire", "parlare", "stareinpiedi", "lavorare", "rompere", "fare", "diventare", "inviare", "comprare", "nuotare", "dormire", "svegliare", "lavarec", "provare", "tentare", "trovare", "portare", "celebrare", "sorridere", "piangere", "costare", "imparare", "insegnare", "scrivere", "cambiare", "cadere", "sentire", "ascoltare", "mostrare", "vincere", "pensare", "chiedere", "aprire", "sapere", "conoscere", "cantare", "ballare", "chiedere", "rispondere", "raccogliere", "amare", "disegnare", "scegliere", "mangiare", "bere", "dare", "cuocere", "infornare", "tagliare", "prendere", "aspettare", "ringraziare", "leggere", "giocare", "ciao", "grazie", "scusi", "perfavore", "prego", "peccato", "arrivederci", "ciao", "grazie", "scusi", "perfavore", "prego", "peccato", "arrivederci"};
    private static String[] es = {"de", "del", "a", "il", "los", "las", "el", "la", "un", "gente", "persona", "hombre", "marido", "mujer", "niño", "muchachoamigo", "invitado", "familia", "padres", "padre", "madre", "hijoabuelosuegro", "tíohermanoprimotrabajo", "maestro", "conductor", "obrero", "ingeniero", "doctor", "enfermera", "vendedor", "contador", "pintor", "estudiante", "país", "Rusia", "España", "animal", "gato", "perro", "pájaro", "ciudad", "escuela", "teatro", "calle", "plaza", "lugar", "casa", "iglesia", "río", "café", "hotel", "jardín", "parque", "banco", "parada", "cine", "puente", "intersección", "bosque", "hospital", "mercado", "policía", "oficina", "de", "correos", "estación", "centro", "tienda", "montaña", "apartamento", "cocina", "balcón", "cuarto", "de", "baño", "ducha", "inodoro", "suelo", "techo", "piso", "pasillo", "dormitorio", "sala", "de", "estar", "puerta", "ventana", "llave", "cama", "almohada", "mesa", "silla", "sillón", "nevera", "sofá", "espejo", "comida", "pan", "mantequilla", "queso", "salchichón", "aceite", "pimienta", "sal", "baya", "miel", "mermelada", "seta", "cebollabanana", "zanahoria", "pera", "remolacha", "fruta", "melón", "sandía", "pastel", "tarta", "chocolate", "carne", "patatas", "ensalada", "tomate", "pepino", "col", "gachas", "sopa", "bocadillo", "soda", "agua", "café", "té", "leche", "jugo", "manzana", "uvas", "naranja", "piña", "albaricoque", "azúcar", "arroz", "fideos", "res", "cerdo", "pollo", "chuleta", "limón", "guisante", "bollo", "pescado", "caramelo", "helado", "nuez", "huevo", "melocotón", "taza", "vaso", "plato", "cuchara", "tenedor", "cuchillo", "platillo", "botella", "servilleta", "desayuno", "almuerzo", "cena", "avión", "coche", "tranvía", "autobús", "tren", "bicicleta", "tiempo", "año", "semana", "hora", "minuto", "ayer", "hoy", "mañana", "fiesta", "vez", "día", "tarde", "noche", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo", "mes", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre", "primavera", "verano", "otoño", "invierno", "nombre", "dirección", "número", "cumpleaños", "casadocosa", "pluma", "libro", "ajedrez", "teléfono", "reloj", "peine", "televisor", "plancha", "jabón", "radio", "bolsa", "mapa", "maleta", "regalo", "cámara", "reproductor", "ordenador", "película", "flor", "florero", "cuadro", "pañuelo", "bola", "globo", "juguete", "cuenta", "sobre", "papel", "periódico", "carta", "billete", "ropa", "zapatos", "abrigo", "vestido", "camisa", "falda", "guante", "sombrero", "chaqueta", "bufanda", "calcetín", "suéter", "camiseta", "corbata", "pantalones", "qué", "cual", "quién", "dónde", "dónde", "de", "dónde", "cómo", "por", "qué", "cuándo", "yo", "tú", "él", "ella", "ustedes", "nosotros", "vosotros", "ellos", "mi", "tu", "su", "nuestro", "vuestro", "de", "sobre", "debajo", "de", "detrás", "de", "con", "sin", "antes", "de", "después", "de", "delante", "de", "entre", "en", "cerca", "de", "para", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve", "diez", "once", "doce", "trece", "catorce", "quince", "dieciséis", "diecisiete", "dieciocho", "diecinueve", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa", "ciento", "mil", "viejo-joven", "nuevo", "grande", "pequeño", "hambriento", "bueno", "malo", "bien", "mal", "temprano", "tarde", "pasado", "próximo", "libre", "caliente", "frío", "alto", "bajo", "largo", "corto", "fácil", "difícil", "ligeropesadooscuro", "claro", "caro", "barato", "la", "izquierdac", "la", "derecha", "correcto", "rápido", "despacio", "suave", "duro", "bonito", "atento", "triste", "alegre", "feliz", "listo", "enojado", "principal", "negro", "azul", "marrón", "verde", "gris", "rojo", "blanco", "amarillo", "sí", "no", "este", "ese", "aquelese", "queaquí", "allí", "acá", "allá", "ahora", "ya", "todavía", "mucho", "poco", "muy", "cada", "todos", "todo", "tan", "pero", "porque", "ser", "estar", "tener", "poder", "deber", "vivir", "permanecer", "venir", "ir", "ver", "sentar", "decir", "hablar", "trabajar", "romper", "hacer", "enviar", "comprar", "nadar", "dormir", "despertar", "lavarc", "probar", "encontrar", "traer", "celebrar", "sonreír", "llorar", "costar", "aprender", "enseñar", "escribir", "cambiar", "caer", "escuchar", "mostrar", "ganar", "pensar", "cerrar", "abrir", "saber", "conocer", "cantar", "bailar", "preguntar", "contestar", "recoger", "amar", "dibujar", "elegir", "querer", "comer", "beber", "dar", "hornear", "cocinarcortar", "tomar", "esperar", "leer", "jugar", "gracias", "perdón", "por", "favorde", "nada", "qué", "lástima", "hasta", "la", "vista", "canción", "música", "tamaño", "dinero", "felicidad", "suerte", "broma", "sorpresa", "problema", "lluvia", "viento", "nieve", "cielo"};
    private static String[] de = {"ein", "eine", "eines", "einer", "einem", "einer", "einen", "von", "außer", "entgegen", "seit", "für", "über", "neben", "um", "bis", "nach", "bei", "die", "das", "des", "der", "den", "dem", "wer", "was", "wie", "auf", "ach", "Leben", "lesen", "machen", "denken", "reisen", "spielen", "warten", "werden", "wohnen", "Alter", "wollen", "Eltern", "Familie", "Frau", "Kind", "Jahr", "Tag", "Monat", "ich", "Welt", "weiter", "alles", "weit", "bald", "Seite", "durch", "Zahl", "alt", "Arbeit", "frei", "immer", "klein", "du", "neu", "verheiratet", "verwitwet", "da", "erst", "etwa", "etwas", "hier", "jetzt", "lei", "noch", "kein", "schon", "übrigens", "aber", "aus", "es", "dein", "von", "was", "wieviel", "so", "Wiedersehen", "schön", "stehen", "Abend", "zur", "Zeit", "Mann", "Männer", "fest", "nicht", "Lehrer", "und", "Land", "Län", "auch", "später", "dort", "kaufen", "verkaufen", "zusammen", "dann", "Stunde", "möglich", "Liebe", "liegen", "antworten", "kommen", "entschuldigen", "funktionieren", "kosten", "sagen", "spülen", "stimmen", "waschen", "wechseln", "Antwort", "Benzin", "Bett", "Bild", "Fehler", "heißen", "Geld", "Geschäft", "Haus", "Häuser", "Haushalt", "Herd", "	", "Idee", "können", "schnell", "sein", "Regal", "anders", "in", "haben", "Stuhl", "Stühle", "Tisch", "Topf", "Töpfe", "Uhr", "Sonne", "gehen", "Zeit", "ähnlich", "sehen", "ehrlich", "kaputt", "groß", "lustig", "originell", "sehr", "heute", "viel", "o", "sondern", "zu", "raus", "danke", "bitte", "Lampe", "trinken", "essen", "Bleistift", "ordnen", "Beruf", "Arzt", "Ärzte", "Baum", "Bäume", "nach", "gegen", "als", "Auto", "er", "bestellen", "bezahlen", "brauchen", "genau", "glauben", "kochen", "mögen", "über", "üben", "Abendessen", "Anzeige", "Apfel", "Äpfel", "müssen", "Brot", "zwischen", "Butter", "unter", "Ei", "Eier", "Eis", "Erdbeere", "Flasche", "Fleisch", "Frage", "selbst", "Frühstück", "Gabel", "Zeitung", "Gemüse", "Gericht", "Gespräch", "Getränk", "Gewürz", "Glas", "Gläser", "Prozent", "Käse", "Kuchen", "Löffel", "mehr", "Messer", "Nachtisch", "Öl", "Pfeffer", "Blume", "Preis", "Reis", "Saft", "Säfte", "Sahne", "Schinken", "Soße", "langsam", "Wasser", "Suppe", "Tasse", "Teller", "Tomate"};

    private static Dictionary[] dictionaries = {
            new Dictionary("en", en),
            new Dictionary("fr", fr),
            new Dictionary("it", it),
            new Dictionary("es", es),
            new Dictionary("de", de),
    };

    static Message.Type detectType(String message) {
        if (isUrl(message))
            return Message.Type.LINK;
        else
            return Message.Type.TEXT;

    }

    private static boolean isUrl(String url) {
        final UrlValidator validator = new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES);
        return validator.isValid(url) || validator.isValid("http://" + url);
    }

    static String detectText(String message, Message.Type type) throws IOException, ExecutionException, InterruptedException {
        switch (type) {
            case LINK:
                final String finalUrl;

                if (!new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES).isValid(message))
                    finalUrl = "https://" + message;
                else
                    finalUrl = message;

                ScannedCodeActivity.titleRetriever.execute(finalUrl);
                final Document document = ScannedCodeActivity.titleRetriever.get();
                if (document == null)
                    return null;
                final String title = document.title();
                final String lang = document.select("html").first().attr("lang");
                if (lang != null && !lang.equals(""))
                    probableLocale = new Locale(lang);
                return title;
            default:
                return message;
        }
    }


    static Locale detectLang(String message) {
        if (message == null)
            return context.getResources().getConfiguration().locale;


        final String garbage = "!@#$%^&*()_+|\\/.?>';\":0123456789 \t\n";
        int nGarbage = 0;

        final String[][] alpha = {
                //alpha, typical, not typical, name
                {"", "", "", "ru"},
                {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", "", "", "en"},
                {"abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ", "", "jkwxyJKWXY", "it"},
                {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", "ÄäÖöÜüẞß", "", "de"},
                {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", "ÑñÓóÉéÍíÚúÜüÁá", "", "es"},
                {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", "ÉéÂâÊêÎîÔôÛûÀàÈèÙùËëÏïÜüŸÿÇç", "", "fr"},
//                {"一丁丂七丄丅丆万丈三上下丌不与丏丐丑丒专且丕世丗丘丙业丛东丝丞丟丠両丢丣两严並丧丨丩个丫丬中丮丯丰丱串丳临丵丶丷丸丹为主丼丽举丿乀乁乂乃乄久乆乇么义乊之乌乍乎乏乐乑乒乓乔乕乖乗乘乙乚乛乜九乞也习乡乢乣乤乥书乧乨乩乪乫乬乭乮乯买乱乲乳乴乵乶乷乸乹乺乻乼乽乾乿亀亁亂亃亄亅了亇予争亊事二亍于亏亐云互亓五井亖亗亘亙亚些亜亝亞亟亠亡亢亣交亥亦产亨亩亪享京亭亮亯亰亱亲亳亴亵亶亷亸亹叼叽叾叿吀吁吂吃各吅吆吇合吉吊吋同名后吏吐向吒吓吔吕吖吗吘吙吚君吜吝吞吟吠吡吢吣吤吥否吧吨吩吪含听吭吮启吰吱吲吳吴吵吶吷吸吹吺吻吼吽吾吿呀呁呂呃呄呅呆呇呈呉告呋呌呍呎呏呐呑呒呓呔呕呖呗员呙呚呛呜呝呞呟呠呡呢呣呤呥呦呧周呩呪呫呬呭呮呯呰呱呲味呴呵呶呷呸呹呺呻呼命呾呿咀咁咂咃咄咅咆咇咈咉咊咋和咍咎咏咐咑咒咓咔咕咖咗咘咙咚咛咜咝咞咟咠咡咢咣咤咥咦咧咨咩咪咫咬咭咮咯咰咱咲咳咴咵咶咷咸咹咺咻咼咽咾咿哀品哂哃哄哅哆哇哈哉哊哋哌响哎哏哐哑哒哓哔哕哖哗哘哙哚哛哜哝哞哟哠員哢哣哤哥哦哧哨哩哪哫哬哭哮哯哰哱哲哳哴哵哶哷哸哹哺哻哼哽哾哿唀唁唂唃唄唅唆唇唈唉唊唋唌唍唎唏唐唑唒唓唔唕唖唗唘唙唚唛唜唝唞唟唠唡唢唣唤唥唦唧唨唩唪唫唬唭售唯唰唱唲唳唴唵唶唷唸唹唺唻唼唽唾唿啀啁啂啃啄啅商啇啈啉啊啋啌啍啎問啐啑啒啓啔啕啖啗啘啙啚啛啜啝啞啟啠啡啢啣啤啥啦啧啨啩啪啫啬啭啮啯啰啱啲啳啴啵啶啷啸啹啺啻啼啽啾啿喀喁喂喃善喅喆喇喈喉喊喋喌喍喎喏喐喑喒喓喔喕喖喗喘喙喚喛喜喝喞喟喠喡喢喣喤喥喦喧喨喩喪喫喬喭單喯喰喱喲喳喴喵営喷喸喹喺喻喼喽喾喿嗀嗁嗂嗃嗄嗅嗆嗇嗈嗉嗊嗋嗌嗍嗎嗏嗐嗑嗒嗓嗔嗕嗖嗗嗘嗙嗚嗛嗜嗝嗞嗟嗠嗡嗢嗣嗤嗥嗦嗧嗨嗩嗪嗫嗬嗭嗮嗯嗰嗱嗲嗳嗴嗵嗶嗷嗸嗹嗺嗻嗼嗽嗾嗿嘀嘁嘂嘃嘄嘅嘆嘇嘈嘉嘊嘋嘌嘍嘎嘏嘐嘑嘒嘓嘔嘕嘖嘗嘘嘙嘚嘛嘜嘝嘞嘟嘠嘡嘢嘣嘤嘥嘦嘧嘨嘩嘪嘫嘬嘭嘮嘯嘰嘱嘲嘳嘴嘵嘶嘷嘸嘹嘺嘻嘼嘽嘾嘿噀噁噂噃噄噅噆噇噈噉噊噋噌噍噎噏噐噑噒噓噔噕噖噗噘噙噚噛噜噝噞噟噠噡噢噣噤噥噦噧器噩噪噫噬噭噮噯噰噱噲噳噴噵噶噷噸噹噺噻噼噽噾噿嚀嚁嚂嚃嚄嚅嚆嚇嚈嚉嚊嚋嚌嚍嚎嚏嚐嚑嚒嚓嚔嚕嚖嚗嚘嚙嚚嚛嚜嚝嚞嚟嚠嚡嚢嚣嚤嚥嚦嚧嚨嚩嚪嚫嚬嚭嚮嚯嚰嚱嚲嚳嚴嚵嚶嚷嚸嚹嚺嚻嚼嚽嚾嚿囀囁囂囃囄囅囆囇囈囉囊囋囌囍囎囏囐囑囒囓囔囕囖囗囘囙囚四囜囝回囟因囡团団囤囥囦囧囨囩囪囫囬园囮囯困囱囲図围囵囶囷囸囹固囻囼国图囿圀圁圂圃圄圅圆圇圈圉圊贾贿赀赁赂赃资赅赆赇赈赉赊赋赌赍赎赏赐赑赒赓赔赕赖赗赘赙赚赛赜赝赞赟赠赡赢赣赤赥赦赧赨赩赪赫赬赭赮赯走赱赲赳赴赵赶起赸赹赺赻赼赽赾赿趀趁趂趃趄超趆趇趈趉越趋趌趍趎趏趐趑趒趓趔趕趖趗趘趙趚趛趜趝趞趟趠趡趢趣趤趥趦趧趨趩趪趫趬趭趮趯趰趱趲足趴趵趶所以學校會以粵語授課趷趸趹趺趻趼趽趾趿跀跁跂跃跄跅跆跇跈跉跊跋跌跍跎跏跐跑跒跓跔跕跖跗跘跙跚跛跜距跞跟跠跡跢跣跤跥跦跧跨跩跪跫跬", "", "", "zh"}
                {"的一A壹是不了人我在有他这這中大来來上国國个個,箇到说說们們为為子和你地出道也时時年得就那要下以生会會自着著去之过過家学學对對可她里后小么心多天而能好都然没沒日于起还還发成事只祇作当想看文无無开開手十A拾用主行方又如前所本见見经經头頭面公同三A叁已老从從动動两兩长長知民样樣现現分将將外但身些与與高意进進把法此实實回二A贰理美点點月明其种種声聲全工己话話儿兒者向情部正名定女问問力机機给給等几很业業最间間新什甚打便位因重被走电電四A肆第门門相次东東政海口使教西再平真听聽世气氣信北少关關并内內加化由却卻代军軍产產入先山五A伍太水万萬市眼体體别別,彆处處总總才纔场場师師书書比住员員九A玖笑性通目华華报報立马馬命张張活难難神数數件安表原车車白应應路期叫死常提感金何更反合放做系计計或司利受光王果亲親界及今京务務制解各任至清物台象记記边邊共风風战戰干接它许許八A捌特觉覺望直服毛林题題建南度统統色字请請交爱愛让讓认認算论論百佰吃义義科怎元社术術结結六A陆,陸功指思非流每青管夫连連远遠资資队隊跟带帶花快条條院变變联聯言权權往展该該领領传傳近留红紅治决決周保达達办辦运運武半候七A柒必城父强強,彊步完革深区區即求品士转轉量空甚众眾技轻輕程告江语語英基派满滿式李息写寫呢识識极極令黄黃德收脸臉钱錢党黨倒未持取设設始版双雙历越史商千仟片容研像找友孩站广廣改议議形委早房音火际際则則首单單据據导導影失拿网網香似斯专專,耑石若兵弟谁誰校读讀志飞飛观觀争爭究包组組造落视視济濟喜离離虽雖坐集编編宝寶谈談府拉黑且随隨格尽剑劍讲講布杀殺微怕母调調局根曾准团團段终終乐樂切级級克精哪官示冲竟乎男举舉客证證,証苦照注费費足尔爾招群热熱推晚响響称稱兴興待约約阳陽哥惊驚吗嗎整支古汉漢突号號绝絕选選吧参參刊亚亞复伤傷类類备備欢歡另港势勢刻星断斷陈陳掌农農夜般念唸价脑腦规規底故省妈媽刚剛句显顯消衣陆陸器确確破具居批送泽澤紧緊帮幫线線存愿願奇害增杨楊料州节節左装裝易著急久低岁歲需酒河初游严嚴铁鐵族除份敢胡血企仍投闻聞斗纪紀脚腳右苏蘇标標饭飯云病医醫阿答土况況境软軟考娘村刀击擊仅僅查引朝育续續独獨罗羅买買户戶护護喝朋供责責项項背余希卫衛列图圖室乱亂刘劉爷爺龙龍咱章席错錯兄暗创創排春须承案忙呼树樹痛沉啊灵靈职職乡鄉细細诉訴态態停印笔筆夏助福块塊冷球姑划既质質巴致湾灣演木韦韋怪围圍静靜旁园園否副辑輯采食登够夠赛賽米假较較姐楼樓获孙孫宣穿诗詩歌速忽堂敌敵试試谢謝央怀懷顾顧验驗营營止姓养養丽麗属屬景郭依威按恶惡慢座罪维維渐漸胜勝藏皇街激异異摘角瞧负負施模草某银銀露阵陣值班层層修差味织織药藥馆館密亮律习習田简簡免毒归歸波型屋换換救寄帝退洋丝絲湖睡劳勞妇婦伯尼皮祖雄婚康评評追哈络絡店翻环環礼禮跑超叶压壓占均永烈奖獎婆赶趕富兰蘭录錄画畫遇顿頓艺藝普判源亿億素船继繼尚嘴察雨优優您险險烟阶階担擔散板钟访訪妹伸佛限讨討临臨吴吳摇搖跳曲练練构構玩玉犯厂廠肯协協幸挥揮效齐齊封温溫疑肉攻纸紙策充顶頂寻尋宁寧沙防抓例股卖賣顺順警梦夢剧劇善蒙票良范坚堅端靠杂雜贵貴怒稿拍率旧舊掉啦莫授守油恩积積益县縣哭罢罷庭窗扬揚忘午卡雪菜牌牛脱脫博丈弹彈洲松坏邓鄧鲜鮮短毕畢置楚欧歐略智岛島抗妻抱载載败敗枪槍适適虚虛预預睛刺爹纷紛介括销銷降鱼魚奔忍宗盘盤耳野讯訊配禁索赵趙默徒架灯燈峰状狀款移爸托洪升伙订訂毫狐镇鎮床互套旅逃骂罵输輸唱靖秘词詞困泪淚熟财財鬼骨申欲征私舞秋巨迎秀搞丁吸审審遍墙牆朱圣聖避跃躍忌桌执執悲域晓曉弄亡桥橋辈輩闪閃隐隱劲勁闹鬧恐呀付敬监監厅廳库庫震材冰醒庆慶绿綠腿述徐尊硬额額误誤借纳納折售遗遺暴缺迷鲁魯探货貨童缓緩伟偉君庄莊凡危烧燒彩抢搶控胸戏戲篇趣束谓謂概射课課洞麻杯透邮郵荣榮懂拥擁献獻洗休迫叹嘆狗偷阴陰汽拜横橫鼓健厚签簽丹洛喊蓉轮輪岸奶淡潮训訓圆圓卷释釋诸諸妙唯夺奪逐燕呆测測浪抽盖蓋偏阅閱购購途纵縱耶摸挂掛航择擇恨舍拳竹唐誉譽乘弱检檢宫宮仪儀旗含袁址摆擺奥奧番混灭滅握牙虑慮召猛宽寬盛核袋绍紹补補典圈丰雅吉赞贊茶亦谷稳穩汇厉厲届屆迹跡雷序寒附鸡雞遭挑肩忆憶柔戴惜隔豪诚誠瑞减減播麼针針棋竞競臂挺操腰狂替梅固伦倫宋钢鋼诺諾残殘延虎迅灾災悄岳乔喬川仇季吹粮糧聚译譯珠叔谋謀础礎仁损損融辆輛净凈敏伊仙巧零累享伴荡蕩珍勇末奋奮胆膽弃棄烦煩糊犹猶税稅培抵僧锋鋒乃遥遙摩坦後S后眉餐惯慣凭憑冠抬碰币幣启啟码碼冒汗俗灰督穷窮颇頗倾傾尖韩韓贸貿仿孤飘飄漫予紫侧側沿拔袖梁赏賞幕壁旦晨纯純闭閉凉涼扫掃尤炮碗贴貼插滚滾缘緣寺贝貝润潤氏冬扩擴栏欄荒哲逼吓嚇堆撞郎俩倆蓝藍闲閑辛镜鏡陪骑騎蛋促羊宜嘛颜顏贫貧幅驻駐萍污杰傑扑撲壮壯萨薩刑忧憂貌狱獄塞凤鳳孔触觸恋戀岂豈森繁碎津侠俠隆迟遲辉輝狠析缩縮穴萧蕭怨磨伏辞辭泥龄齡径徑鼻赖賴仰愤憤慕俄映询詢惨慘麦麥宿倍粗腾騰稍截染乌烏愈岗崗柳铺鋪涉疾挡擋奉踏忠伍躲籍努朗箱裁帐帳兼彼霞猪豬悉扎劝勸薄筑築俊鞋距侵欣挤擠媒吐魂洁潔枝盈阻陷甲郑鄭鸣鳴倘剩颗顆拖舒惠昏振戒丧喪焦爬凌慧偶晃桃赤烂爛骗騙措页頁凶泰尸屍坡勒疗療塔尘塵躺殊慰坛甘咬拒彪炸井崇饮飲祝汪牢桂尾漂聊撒恰凝矛於宾賓绪緒彭肚匆描粉贼賊乏盾愁斜裂滑斐废廢寂涌详詳汤湯彻徹玄斤轰轟奸怜憐朵佳皆鸟鳥邦扶毁毀聪聰辩辯瓶饿餓蛇捕搬沈枫楓舅幽魔琴挣掙聘弯彎墓欺悟蒋蔣臣返违違亏虧丢丟援赫魏耐佩酸盟胖傻绕繞哼秦屈辱昨瘦暂暫箭署赴递遞猜潜潛鸿鴻绩績耀涂塗割豆闷悶亭祥励勵宇泛狼悔搭舰艦浮牵牽符肃肅奴爆姊幼夹夾邀疯瘋允恼惱租椅尺侍腐颤顫扭菲瓦擦辣奏殷埋摔盼吟渡衫跨醉艰艱掩荷苍蒼旋揭桑仗莲蓮钻鑽宏幻刃峡峽辽遼娃凯凱患障丐衡猫貓丑涛濤暖溜锅鍋奈罚罰拾浓濃键鍵脉脈锁鎖邻鄰臭芳垂捉恢姆砍驾駕恭挨祸禍曹慈抖泉览覽澳脏疼铜銅羞档檔抛拋苗惑肥驱驅窝窩裤褲估胞柄阔闊杜勤舟帽玛瑪撤频頻禅禪柯莱萊堪寸哀熊腹尝嘗敲勃巡盗盜筹籌扣池浅淺柴埃嫁枚稀雕厌厭瓜寿壽跌扯董锦錦鉴鑒刷趋趨捐傲贯貫殿拨撥逊遜踢赔賠姿迁遷黎祭滴袭襲慌鞭茫逢屠昆柏驶駛咐植惧懼纽紐捷圳牲踪蹤丛叢漠锐銳喇乾霍湿濕睁睜仔吵悠沟溝墨串俱陶贡貢浑渾赢贏屁驰馳棒匹拼恒恆邪脆糟扮贤賢饰飾偿償拆缠纏摄攝拟擬滋嘿旨闯闖贺賀翠缝縫饱飽劫抚撫挖册冊叛肖熙炼煉宪憲庙廟碧盒谊誼储儲冯馮唤喚贪貪卧臥翼扰擾胁脅跪碑呵甜洒谱譜莎娜阁閣庸鹏鵬覆玲侯抹卢盧碍礙综綜丘晕暈拦攔燃昌吞嫌狄押舌琳雾霧曼耻恥柜摊攤削戚杆岩喂扔逝诞誕悬懸爽崔廷凑湊痴盆御酷艾唉姥笼籠颠顛姻携攜愧芬穆扇郁掷擲怔芯鼠纠糾疆曰傅袍唇稣穌捧勾牧儒慨筋柱卑咽吨噸虫蟲绳繩厨廚冤涨漲皱皺疲赌賭饶饒矿礦畅暢煤腕喷噴遣浩翁咨镖鏢屏仲嘻吩棉孟撑撐炉爐泄葬搜添遵迪伪偽兆欠谅諒炎氛杖瞎钓釣肠腸披剥剝誓赚賺役泡逆矮吊填嘉烛燭厦廈夕衰液薛仆迈邁齿齒谨謹呈昂抄弥彌渴梯疏耗瞪斥夸誇蒂剪娶痕弗姚债債妥璃掏刹剎晶衷肤膚鹿拓卓症癥糖钦欽绵綿哩诱誘枯歇塑妨豫抑珊棍晋晉淋悦悅敦艳艷玻砸嚷盲辨葛罕矩泳宅贷貸膀捏颈頸践踐脖贾賈轿轎脾堡娇嬌浙劣潇瀟赐賜陀蓄坟墳颂頌漏杭茅砖磚瞬鹤鶴辟渔漁乖霸襄炒哑啞浦饼餅牺犧滩灘钉釘吁锡錫赠贈哄铃鈴顽頑殖鹰鷹蔡催芙彬拚轨軌哟喲歉盯硕碩惹契愚帅帥惶憾懒懶姨喘兽獸陌罩猎獵嵩盐鹽饥飢凄喉宴腔翰膝陵蜂逻邏劈廉裹骄驕贩販绘繪崖辰涯戈坑遮擒蛮蠻芷堵雇挽眠吻孝泊撕虹叙敘粹勉竭歪慎棵朴械溪莉斑磕寡循斩斬掠吕呂昔郊爵徽磁俯谭譚鼎拂俺嫂帕嗯冻凍婉桐骆駱泼潑匠艇谦謙妓菩厕廁俘毅岭嶺丫畏湘桶嗓煌鲍鮑粒巷帘秃禿腊仓倉拐绑綁啥荐薦倪瑟廊鸭鴨蜜诊診棚掀筒媳纹紋秒沾庞龐蹲骚騷歧艘芝哗嘩亩畝券趟巾淫谎謊寞灌篮籃妄搁擱侄厢廂叉俞伐宰瞒瞞宙肿腫漆怖吾吼侨僑叠疊啸嘯罐肆裙泣胀脹赋賦熬趁咳愉恳懇辜肌婴嬰羽躬毙斃拘叮哇晴谜謎淮旺逸琼瓊姜呜嗚窜竄颁頒薪寨尿颊頰逮卜昭浸刮宛嘱囑囊寓驳駁倡浴咕挪搏蓬晌渠兜喃夷沪滬贱賤魄舵晰僵糕裔秩倚塌恍钩鉤嘲傍裕煮乳勿竖豎惩懲睹株绣繡妖讶訝咖纲綱胎滨濱耕嗤舱艙娱娛匪鸦鴉胃躁狮獅砰妮凛凜龟龜裸嫣甫窑窯塘纤纖宠寵链鏈拱尹掘坝壩狭狹铭銘淳沐馨潘甩榜渊淵羡侮卿兀喧履猴枉衬襯畔凳缅緬弦畜粞溢搂摟乞旬缚縛灿燦舆輿雁倦酬韵韻媚堤攀窃竊嫩遂澄侦偵陕陝陋笨匙沫耸聳踩酱醬壶壺啡碌痒癢鄙壳殼贞貞霉蠢芦蘆胳矣焰脊囚辅輔账賬佐僚雀撰耍枕捡撿涵逗粪糞朦肝蒸滥濫筷溃潰隶隸烤缸弓潭旷曠哎峻爪怯茂芒肢稻兔圾喻框缴繳蹈哨颖穎菊喀妆妝淹瓷淀蜡蠟嚼剂劑逛骤驟暑襟庐廬苹蘋晒曬悼昧拢攏函胧朧胶膠抒乒讽諷歹旱葡惟拣揀耿廿桩樁乙谣謠坠墜滞滯孕诵誦梳冈岡肺丸霜汁纱紗衔銜腻膩甸啤坎稼禾愣脂萄捞撈搅攪屑伞傘蝶铸鑄躯軀稚腥藤陡烫燙梨哦浆漿僻坊焉隙淘垮滔酿釀鹅鵝兹茲捣搗栋棟瑰敞癌缆纜烁爍玫诈詐煞膜焚粘摧疫幢汝毯挫纺紡朽锤錘兑兌辫辮堕墮笛觅覓蔽谐諧氓蔑沸藉卸熄扁炭慷篷眨譬叨蔬绸綢婿寥浇澆乓膏膛琢啪淑叭弊灶醋斌奠屯膨沦淪缕縷壤冶暇揉萝蘿翔蛛栗荫蔭讥譏葱蔥巩鞏粥斋齋迄帜幟菌铅鉛贿賄绒絨侣侶锻鍛谴譴汹洶敷宵讳諱锣鑼撼亨淌扛杉燥匀勻渺碟嵌沃剖姬绰綽嗦绞絞轴軸垒壘噪蕴蘊邵咋坪佣傭卵昼晝憋奎捂煎瞅蚀蝕熔虾蝦谬謬绅紳镶鑲聋聾募垄壟彦彥翘翹趴杏彰阐闡讼訟枢樞崭嶄蒲泻瀉俭儉橡溶瀑扒琛", "", "", "zh"}
        };
        final float[] probabilities = new float[alpha.length];
        final int[][] amounts = new int[alpha.length][3];

        for (int i = 0; i < message.length(); i++) {
            if (garbage.contains("" + message.charAt(i))) {
                nGarbage++;
                continue;
            }
            for (int j = 0; j < alpha.length; j++) {
                String[] langChars = alpha[j];

                if (langChars[PLAIN].contains("" + message.charAt(i)))
                    amounts[j][PLAIN]++;
                if (langChars[TYPICAL].contains("" + message.charAt(i)))
                    amounts[j][TYPICAL]++;
                if (langChars[NOT_TYPICAL].contains("" + message.charAt(i)))
                    amounts[j][NOT_TYPICAL]++;
            }
        }
        for (int i = 0; i < alpha.length; i++) {
            probabilities[i] = (amounts[i][PLAIN] + 2.0f * amounts[i][TYPICAL] - 5.0f * amounts[i][NOT_TYPICAL]) / (message.length() - nGarbage);
        }

        int nMax = 0;
        float max = 0.0f;
        for (int i = 0; i < alpha.length; i++) {
            if (probabilities[i] > max) {
                max = probabilities[i];
                nMax = i;
            }
        }

        List<Integer> mostPossible = new ArrayList<>();
        for (int i = 0; i < probabilities.length; i++) {
            if (abs(probabilities[i] - max) < 0.00001)
                mostPossible.add(i);
        }

        int mostPossibleLang = nMax;
        float maxPossibility = 0.0f;
        if (mostPossible.size() > 1) {
            List<String> lexemes = Arrays.asList(message.split("[" + garbage + "]"));

            for (int i = 0; i < mostPossible.size(); i++) {
                Dictionary dictionary = getLang(alpha[mostPossible.get(i)][LANG]);
                if (dictionary == null)
                    break;
                float possibility = dictionary.findPossibility(lexemes);
                if (possibility > maxPossibility) {
                    maxPossibility = possibility;
                    mostPossibleLang = mostPossible.get(i);
                }
            }
            System.out.println(alpha[mostPossibleLang][LANG]);
            return new Locale(alpha[mostPossibleLang][LANG]);
        }

        if (max > 0.8) {
            System.out.println(alpha[nMax][LANG]);
            return new Locale(alpha[nMax][LANG]);
        }else
            return context.getResources().getConfiguration().locale;
    }

    private static Dictionary getLang(String wantedLang) {
        for (Dictionary dict : dictionaries) {
            if (dict.getLang().equalsIgnoreCase(wantedLang))
                return dict;
        }
        return null;
    }



}

/*


*/