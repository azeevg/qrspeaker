package com.qrspeaker;

import android.os.AsyncTask;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class TitleRetriever extends AsyncTask<String, Void, Document> {
    @Override
    protected Document doInBackground(String... message) {
        if (message == null || message.length == 0)
            return null;

        try {
            return Jsoup.connect(message[0]).timeout(5000).followRedirects(true).get();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
