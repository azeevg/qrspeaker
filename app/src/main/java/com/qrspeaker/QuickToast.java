package com.qrspeaker;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class QuickToast extends Toast {
    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public QuickToast(Context context) {
        super(context);
    }


    public static Toast makeText(Context context, int resId, int duration, int fontSize) {
        final Toast toast = Toast.makeText(context, resId, duration);
        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(fontSize);

        return toast;
    }
}
