package com.qrspeaker;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.speech.tts.TextToSpeech;

import java.util.Locale;


public class TtsWrapper {
    public static final String TTS_URI = "com.google.android.tts";
    private static final String TTS_ENGINE_WAS_NOT_INITED = "TTS engine wasn't initialized";
    private final TextToSpeech tts;
    private final Context context;

    public TtsWrapper(final Context context) {
        this.context = context;

        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    tts.setLanguage(context.getResources().getConfiguration().locale);
                }
            }
        });
    }

    public static boolean isAppInstalled(String uri, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public int setLanguage(Locale locale) throws Exception {
        if (tts == null)
            throw new Exception(TTS_ENGINE_WAS_NOT_INITED);
        return tts.setLanguage(locale);
    }

    public boolean isSpeaking() {
        return tts.isSpeaking();
    }

    public void stopSpeaking() {
        tts.stop();
    }

    public int speak(String text) throws Exception {
/*

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            System.out.println(tts.getEngines());
        }
*/

        if (tts == null)
            throw new Exception(TTS_ENGINE_WAS_NOT_INITED);
        return tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    public boolean isTtsReady() {
        return isAppInstalled(TTS_URI, context);
    }

    public void setSpeed(float rate) {
        tts.setSpeechRate(rate);
    }
}
