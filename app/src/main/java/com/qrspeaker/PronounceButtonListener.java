package com.qrspeaker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import static com.qrspeaker.TtsWrapper.TTS_URI;
import static com.qrspeaker.TtsWrapper.isAppInstalled;

public class PronounceButtonListener implements View.OnClickListener {
    private final Context context;
    private final String textToSpeak;
    private final TtsWrapper tts;
    private final Locale lang;
    private AlertDialog dialog;

    public PronounceButtonListener(Context context, AlertDialog dialog, String textToSpeak, TtsWrapper tts, Locale lang) {
        super();

        if (tts.isSpeaking())
            tts.stopSpeaking();

        tts.setSpeed(0.9f);

        this.context = context;
        this.dialog = dialog;
        this.textToSpeak = textToSpeak;
        this.tts = tts;
        this.lang = lang;
    }

    private static void setFontSize(AlertDialog dialog, float size) {
        TextView text;
        Button button;

        text = (TextView) dialog.findViewById(android.R.id.message);
        if (text != null)
            text.setTextSize(size);
        button = (Button) dialog.findViewById(android.R.id.button1);
        if (button != null)
            button.setTextSize(size);
        button = (Button) dialog.findViewById(android.R.id.button2);
        if (button != null)
            button.setTextSize(size);
    }

    @Override
    public void onClick(View v) {
        if (tts.isSpeaking()) {
            tts.stopSpeaking();
            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        if (!tts.isTtsReady() || isOldTtsInstalled(prefs)) {
            Editor editor = prefs.edit().putBoolean("firstTime", true);
            editor.apply();

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                            .setData(Uri.parse("market://details?id=" + TTS_URI));
                    context.startActivity(goToMarket);
                    dialog.dismiss();
                }
            };

            builder.setMessage(R.string.google_tts_not_found)
                    .setPositiveButton(R.string.install, listener)
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            dialog = builder.create();

            dialog.show();
            setFontSize(dialog, 36);

            return;
        }

        int result = 0;
        try {
            result = tts.setLanguage(lang);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
        if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent installTTSIntent = new Intent().setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    context.startActivity(installTTSIntent);
                    dialog.dismiss();
                }
            };

            builder.setMessage(R.string.lang_not_supported)
                    .setPositiveButton(R.string.install, listener)
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            dialog = builder.create();
            dialog.show();
            setFontSize(dialog, 36);
        }

        try {
            if (tts.isSpeaking())
                tts.stopSpeaking();

            tts.speak(textToSpeak);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isOldTtsInstalled(SharedPreferences prefs) {
        if (prefs == null)
            return isAppInstalled("com.svox.pico", context);
        else
            return isAppInstalled("com.svox.pico", context) && !prefs.getBoolean("firstTime", false);
    }
}
