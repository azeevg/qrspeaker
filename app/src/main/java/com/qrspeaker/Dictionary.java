package com.qrspeaker;


import java.util.ArrayList;
import java.util.List;

class Dictionary {
    final String lang;
    final String[] dictionary;

    Dictionary(String lang, String[] dictionary) {
        this.lang = lang;
        this.dictionary = dictionary;
    }

    public String getLang() {
        return lang;
    }

    float findPossibility(List<String> _lexemes) {
        List<String> lexemes = new ArrayList<>(_lexemes);

        int nTypicalWords = 0;

        for (int i = 0; i < dictionary.length; i++) {
            String word = dictionary[i];

            for (int j = 0; j < lexemes.size(); j++) {
                if (lexemes.get(j).equals(word)) {
                    ++nTypicalWords;
//                    System.out.println(lang + ":\t" + lexemes.get(j));
                    lexemes.remove(j);
                    break;
                }
            }
        }

        return 1.0f * nTypicalWords / lexemes.size();
    }
}