package com.qrspeaker;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.qrspeaker.database.DBHelper;
import com.qrspeaker.database.Message;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class HistoryActivity extends FragmentActivity {
    List<Message> messages;
    TtsWrapper tts;
    private int numPages;
    private ViewPager mPager;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private AlertDialog dialog;
    private String currentText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        final DBHelper db = new DBHelper(this);
        messages = db.getAllMessages();
        numPages = messages.size();

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        tts = new TtsWrapper(getApplicationContext());

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mPager.getCurrentItem() == messages.size() - 1)
                    setRightArrowVisibility(INVISIBLE);

                if (mPager.getCurrentItem() != 0)
                    setLeftArrowVisibility(VISIBLE);

                if (mPager.getCurrentItem() == 0)
                    setLeftArrowVisibility(INVISIBLE);

                if (mPager.getCurrentItem() != messages.size() - 1)
                    setRightArrowVisibility(VISIBLE);

                if (tts != null && tts.isSpeaking())
                    tts.stopSpeaking();

                final Button listenButton = (Button) findViewById(R.id.listen);
                listenButton.setOnClickListener(
                        new PronounceButtonListener(HistoryActivity.this,
                                dialog, messages.get(mPager.getCurrentItem()).getText(),
                                tts,
                                messages.get(mPager.getCurrentItem()).getLang()));
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        final Button deleteButton = (Button) findViewById(R.id.delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int current = mPager.getCurrentItem();
                final DBHelper dbHelper = new DBHelper(getApplicationContext());

                if (dbHelper.deleteSavedMessage(messages.get(current))) {
                    removeView(current);
                    numPages = messages.size();
                    mPagerAdapter.notifyDataSetChanged();

                    if (current == numPages)
                        mPager.setCurrentItem(current - 1, false);
                    else
                        mPager.setCurrentItem(current, false);
                }

                if (messages.isEmpty())
                    finish();
            }
        });

        final Button listenButton = (Button) findViewById(R.id.listen);
        listenButton.setOnClickListener(
                new PronounceButtonListener(HistoryActivity.this,
                        dialog, messages.get(mPager.getCurrentItem()).getText(),
                        new TtsWrapper(getApplicationContext()),
                        messages.get(mPager.getCurrentItem()).getLang()));

        if (numPages == 1) {
            setLeftArrowVisibility(INVISIBLE);
            setRightArrowVisibility(INVISIBLE);
        }
        if (mPager.getCurrentItem() == 0) {
            setLeftArrowVisibility(INVISIBLE);
        }

    }

    private void removeView(int current) {
        mPagerAdapter.removeView(mPager, current);
    }

    private void setLeftArrowVisibility(int visibility) {
        ImageButton prevButton = (ImageButton) findViewById(R.id.prev);
        prevButton.setVisibility(visibility);
    }

    private void setRightArrowVisibility(int visibility) {
        ImageButton prevButton = (ImageButton) findViewById(R.id.next);
        prevButton.setVisibility(visibility);
    }

    public void jumpToPrevPage(View view) {
        if (mPager.getCurrentItem() > 0) mPager.setCurrentItem(mPager.getCurrentItem() - 1, true);
    }

    public void jumpToNextPage(View view) {
        if (mPager.getCurrentItem() < messages.size() - 1)
            mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
    }

    public void openMenu(final String text) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this).setCancelable(true);
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog, null);

        currentText = text;

        dialog = builder.create();

        dialog.setView(dialogView, 0, 0, 0, 0);
        dialog.show();
    }

    public void copyText(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copy text", currentText);
        clipboard.setPrimaryClip(clip);
        dialog.dismiss();
        QuickToast.makeText(getApplicationContext(), R.string.qrcode_is_copied, Toast.LENGTH_SHORT, 35).show();
    }

    public void sendText(View view) {
        dialog.dismiss();
        Intent sendIntent = new Intent()
                .setAction(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, currentText)
                .setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Sent text"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }
        private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
            final List<Fragment> pages = new ArrayList<>();


            ScreenSlidePagerAdapter(FragmentManager fm) {
                super(fm);

                final View.OnLongClickListener listener = new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Message m = messages.get(mPager.getCurrentItem());

                        if (m.getType().equals(Message.Type.LINK))
                            openMenu(m.getMessage() + "\n(" + m.getText() + ")");
                        else
                            openMenu(m.getText());
                        return true;
                    }
                };

                for (int i = 0; i < getCount(); ++i) {
                    pages.add(HistoryFragment.newInstance(i, numPages, messages.get(i), listener));
                }
            }

            @Override
            public Fragment getItem(int position) {
                return pages.get(position);
            }

            @Override
            public int getCount() {
                return numPages;
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
            }


            int removeView(ViewPager pager, int position) {

                if (pages.size() == 1) {
                    messages.remove(position);
                    pages.remove(position);
                    return position;
                }

                for (int i = position; i < pages.size() - 1; i++) {
                    final HistoryFragment currFragment = (HistoryFragment) pages.get(i);
                    final HistoryFragment nextFragment = (HistoryFragment) pages.get(i + 1);

                    currFragment.setMessage(nextFragment.getMessage());
                }
                pager.setAdapter(null);
                pages.remove(pages.size() - 1);
                messages.remove(position);

                pager.setAdapter(this);

                return position;
            }
        }
    }
