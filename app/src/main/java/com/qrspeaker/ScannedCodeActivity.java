package com.qrspeaker;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.qrspeaker.database.DBHelper;
import com.qrspeaker.database.Message;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.qrspeaker.Utils.context;

public class ScannedCodeActivity extends AppCompatActivity {

    static TitleRetriever titleRetriever;
    String currentText = null;
    private AlertDialog dialog;
    private TtsWrapper tts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanned_code);

        context = getApplicationContext();
        titleRetriever = new TitleRetriever();

        final String message = getIntent().getStringExtra(MainActivity.EXTRA_MESSAGE);
        final Message.Type type = Utils.detectType(message);
        String text;
        try {
            text = Utils.detectText(message, type);
            if (text == null)
                text = message;
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
            text = message;
        }

        final TextView textView = (TextView) findViewById(R.id.textView);
        final Locale lang;
        switch (type) {
            case LINK:
                if (text.equals(message))
                    textView.setText(message);
                else
                    textView.setText(message + "\n(" + text + ")");

                textView.setClickable(true);
                Linkify.addLinks(textView, Linkify.WEB_URLS);
                textView.setLinkTextColor(getResources().getColor(R.color.white));
                if (Utils.probableLocale != null) {
                    lang = Utils.probableLocale;
                    Utils.probableLocale = null;
                } else {
                    lang = Utils.detectLang(text);
                }
                break;
            default:
                lang = Utils.detectLang(text);
                textView.setText(text);
                break;
        }

        final Button saveButton = (Button) findViewById(R.id.save);
        final String finalText = text;

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DBHelper dbHelper = new DBHelper(getApplicationContext());
                try {
                    dbHelper.openDataBase();

                    Message m = new Message(0, message, type, lang, finalText);
                    if (dbHelper.writeMessage(m)) {
                        QuickToast.makeText(getApplicationContext(), R.string.qrcode_is_saved, Toast.LENGTH_SHORT, 35).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                finish();
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                openMenu(textView.getText().toString());
                return true;
            }
        });

        final Button listenButton = (Button) findViewById(R.id.listen);
        tts = new TtsWrapper(getApplicationContext());
        listenButton.setOnClickListener(
                new PronounceButtonListener(ScannedCodeActivity.this,
                        dialog, text, tts, lang));
    }


    public void openMenu(final String text) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ScannedCodeActivity.this).setCancelable(true);
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog, null);

        currentText = text;
        dialog = builder.create();
        dialog.setView(dialogView, 0, 0, 0, 0);
        dialog.show();
    }


    public void copyText(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("copy text", currentText);
        clipboard.setPrimaryClip(clip);
        dialog.dismiss();

        QuickToast.makeText(getApplicationContext(), R.string.qrcode_is_copied, Toast.LENGTH_SHORT, 35).show();
    }

    public void sendText(View view) {
        dialog.dismiss();
        Intent sendIntent = new Intent()
                .setAction(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, currentText)
                .setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Sent text"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (tts != null && tts.isSpeaking()) {
            tts.stopSpeaking();
        }
    }
}
