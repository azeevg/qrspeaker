package com.qrspeaker.database;

import java.io.Serializable;
import java.util.Locale;

public class Message implements Serializable {

    private final String message;
    private final int id;
    private final Type type;
    private final Locale lang;
    private final String text;

    public Message(int id, String message, Type type, Locale lang, String text) {
        this.id = id;
        this.message = message;
        this.type = type;
        this.lang = lang;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Type getType() {
        return type;
    }

    public Locale getLang() {
        return lang;
    }

    public String getText() {
        return text;
    }

    public enum Type {
        TEXT,
        NUMBER,
        LINK,
        DATE;
    }
}
