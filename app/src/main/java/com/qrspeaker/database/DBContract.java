package com.qrspeaker.database;

import android.provider.BaseColumns;

public class DBContract {
    public DBContract() {
    }

    public static abstract class Messages implements BaseColumns {
        public static final String TABLE = "messages_table";
        public static final String COLUMN_ID = "rowid";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_TEXT = "text";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_LANG = "lang";
    }
}
