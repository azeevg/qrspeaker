package com.qrspeaker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;

public class DBHelper {
    public static final String DB_NAME = "qrspeaker_v1.db";
    private String DB_PATH_NAME = null;
    private final Context context;

    public DBHelper(final Context context) {
        this.context = context;
        final String DB_PATH = context.getFilesDir().getPath() + "/";
        DB_PATH_NAME = DB_PATH + DB_NAME;
    }

    public void openDataBase() throws IOException {
        final SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(DB_PATH_NAME, null);
        db.execSQL(String.format(
                "CREATE TABLE IF NOT EXISTS %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s VARCHAR, %s VARCHAR, %s INTEGER, %s VARCHAR);",
                DBContract.Messages.TABLE,
                DBContract.Messages.COLUMN_ID,
                DBContract.Messages.COLUMN_MESSAGE,
                DBContract.Messages.COLUMN_TEXT,
                DBContract.Messages.COLUMN_TYPE,
                DBContract.Messages.COLUMN_LANG));
        if (db.isOpen())
            db.close();
    }


    public List<Message> getAllMessages() {
        final List<Message> messages = new CopyOnWriteArrayList<>();
        final SQLiteDatabase db = SQLiteDatabase.openDatabase(DB_PATH_NAME, null, SQLiteDatabase.OPEN_READONLY);
        final Cursor cursor = db.query(DBContract.Messages.TABLE,
                new String[]{DBContract.Messages.COLUMN_ID,
                        DBContract.Messages.COLUMN_MESSAGE,
                        DBContract.Messages.COLUMN_TEXT,
                        DBContract.Messages.COLUMN_TYPE,
                        DBContract.Messages.COLUMN_LANG},
                null, null, null, null, null);

        cursor.moveToLast();

        while (!cursor.isBeforeFirst()) {
            final String message = cursor.getString(cursor.getColumnIndex(DBContract.Messages.COLUMN_MESSAGE));
            final int id = cursor.getInt(cursor.getColumnIndex(DBContract.Messages.COLUMN_ID));
            final String text = cursor.getString(cursor.getColumnIndex(DBContract.Messages.COLUMN_TEXT));
            final Locale lang = new Locale(cursor.getString(cursor.getColumnIndex(DBContract.Messages.COLUMN_LANG)));
            final Message.Type type = Message.Type.valueOf(cursor.getString(cursor.getColumnIndex(DBContract.Messages.COLUMN_TYPE)));

            messages.add(new Message(id, message, type, lang, text));
            cursor.moveToPrevious();
        }
        cursor.close();
        db.close();
        return messages;
    }

    public boolean deleteSavedMessage(final Message message) {
        if (message.getMessage() == null)
            return false;
        final SQLiteDatabase db = SQLiteDatabase.openDatabase(DB_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
        final int result = db.delete(DBContract.Messages.TABLE,
                DBContract.Messages.COLUMN_ID + " = '" + message.getId() + "'", null);
        if (db.isOpen())
            db.close();
//        System.out.println("id=" + message.getId() + "\t" + "result=" + result);
        return result != 0;
    }

    public boolean writeMessage(final Message message) {
        final SQLiteDatabase db = SQLiteDatabase.openDatabase(DB_PATH_NAME, null, SQLiteDatabase.OPEN_READWRITE);
        final ContentValues newMessage = new ContentValues();
        newMessage.put(DBContract.Messages.COLUMN_MESSAGE, message.getMessage());
        newMessage.put(DBContract.Messages.COLUMN_LANG, message.getLang().getLanguage());
        newMessage.put(DBContract.Messages.COLUMN_TEXT, message.getText());
        newMessage.put(DBContract.Messages.COLUMN_TYPE, message.getType().toString());
        final int messageId = (int) db.insert(DBContract.Messages.TABLE, null, newMessage);
//        System.out.println("id=" + messageId + " message=" + message);


        db.close();
        return messageId != -1;
    }
}
