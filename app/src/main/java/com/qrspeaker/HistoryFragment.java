package com.qrspeaker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qrspeaker.database.Message;

import java.io.Serializable;

public class HistoryFragment extends Fragment {

    public static final String MESSAGES = "message";
    static final String PAGE_NUMBER = "pageNumber";
    static final String PAGE_AMOUNT = "pageAmount";
    private static View.OnLongClickListener longClickListener;
    private Message message;

    static Fragment newInstance(int pageNumber, int pageAmount, Message messages, View.OnLongClickListener listener) {
        final Fragment pageFragment = new HistoryFragment();
        final Bundle arguments = new Bundle();
        arguments.putInt(PAGE_NUMBER, pageNumber);
        arguments.putInt(PAGE_AMOUNT, pageAmount);
        arguments.putSerializable(MESSAGES, (Serializable) messages);
        pageFragment.setArguments(arguments);
        longClickListener = listener;

        return pageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        message = (Message) getArguments().getSerializable(MESSAGES);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.history_fragment, container, false);
        final TextView messageView = (TextView) rootView.findViewById(R.id.textView);

        if (message == null)
            return rootView;

        messageView.setOnLongClickListener(longClickListener);

        switch (message.getType()) {
            case LINK:
                messageView.setText(message.getMessage() + "\n(" + message.getText() + ")");
                messageView.setClickable(true);
                Linkify.addLinks(messageView, Linkify.WEB_URLS);
                messageView.setLinkTextColor(getResources().getColor(R.color.white));

                break;
            default:
                messageView.setText(message.getMessage());
                break;
        }

        return rootView;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


}
